/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question2;



/**
 *
 * @author mfaizmzaki
 */

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class question2Mapper extends Mapper<Text, Text, Text, ObjectWritable> {
    
    Text page = new Text();
    
    @Override
    public void map(Text key, Text value, Mapper.Context context) throws java.io.IOException, InterruptedException {
        
        
        String URL = key.toString();
        page.set(URL);
        
        PageRank tempRank = new PageRank(value.toString());
        
        for (String u : tempRank.getUrlList()) {
            // output url rank
            double newRank = tempRank.getUrlList().length == 0 ? 0 : tempRank.getRank() / (tempRank.getUrlList().length);
            
            context.write(new Text(u), new ObjectWritable(new DoubleWritable(newRank)));
        }
        
        // output url list
        context.write(page, new ObjectWritable(tempRank.getUrlList()));
    }
}

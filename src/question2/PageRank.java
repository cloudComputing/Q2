/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question2;



/**
 *
 * @author mfaizmzaki
 */

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;


public class PageRank implements Writable {
    private double rank;
    private String[] urlList = new String[0];

    public PageRank() {
    }

    public double getRank() {
	return rank;
    }

    public String[] getUrlList() {
	return urlList;
    }

    public PageRank(String str) {
	super();
	String[] items = str.split(" ");
	this.rank = Double.parseDouble(items[0]);
	if (items.length == 2)
	    this.urlList = items[1].split(",");
    }

    public PageRank(double rank, String[] urlList) {
	super();
	this.rank = rank;
	this.urlList = urlList;
    }

    @Override
    public String toString() {
	String res = "";
	for (String s : urlList) {
	    res += s + ",";
	}
	return rank + " " + res;
    }

    @Override
    public void readFields(DataInput in) throws IOException {
	DoubleWritable dw = new DoubleWritable();
	ArrayWritable aw = new ArrayWritable(Text.class);
	dw.readFields(in);
	aw.readFields(in);
	rank = dw.get();
	urlList = aw.toStrings();
    }

    @Override
    public void write(DataOutput out) throws IOException {
	DoubleWritable dw = new DoubleWritable(rank);
	ArrayWritable aw = new ArrayWritable(urlList);
	dw.write(out);
	aw.write(out);
    }
}

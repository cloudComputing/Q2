/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question2;

/**
 *
 * @author mfaizmzaki
 */


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class question2Driver extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
	int res = 0;
	for (int i = 0; i < 50; i++) {
	    System.out.println("Round" + i);
	    res = ToolRunner.run(new Configuration(), new question2Driver(),args);
	}
	System.exit(res);
    }

    @Override
    public int run(String[] arg0) throws Exception {
	// config a job and start it
	Configuration conf = getConf();
        
	Job job = new Job(conf, "PageRank");
	job.setJarByClass(question2Driver.class);
	
        job.setMapperClass(question2Mapper.class);
	job.setReducerClass(question2Reducer.class);
        
	job.setOutputKeyClass(Text.class);
	job.setOutputValueClass(ObjectWritable.class);
        
	job.setInputFormatClass(KeyValueTextInputFormat.class);
        
        conf.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
        conf.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"));
        conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", " ");
         
	FileInputFormat.addInputPath(job, new Path("hdfs://ip-172-31-11-73.eu-west-1.compute.internal:8020/mfaizmzaki/output"));

	FileSystem fs = FileSystem.get(conf);
	fs.delete(new Path("hdfs://ip-172-31-11-73.eu-west-1.compute.internal:8020/mfaizmzaki/tempOut"), true);
	FileOutputFormat.setOutputPath(job, new Path("hdfs://ip-172-31-11-73.eu-west-1.compute.internal:8020/mfaizmzaki/tempOut"));
	int res = job.waitForCompletion(true) ? 0 : 1;
	if (res == 0) {
	    fs.delete(new Path("hdfs://ip-172-31-11-73.eu-west-1.compute.internal:8020/mfaizmzaki/output"), true);
	    fs.rename(new Path("hdfs://ip-172-31-11-73.eu-west-1.compute.internal:8020/mfaizmzaki/tempOut"), new Path("hdfs://ip-172-31-11-73.eu-west-1.compute.internal:8020/mfaizmzaki/output"));
	}
	return res;
    }

}



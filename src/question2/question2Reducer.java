/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question2;



/**
 *
 * @author mfaizmzaki
 */

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class question2Reducer extends Reducer<Text, ObjectWritable, Text, PageRank> {

    @Override
    public void reduce(Text key, Iterable<ObjectWritable> val, Context context) throws java.io.IOException, InterruptedException {
	
        double dampingFactor = 0.75;
	double rank = 0.0;

	String[] urlList = new String[0];
	for (ObjectWritable values : val) {
	    if (values.getDeclaredClass().toString().equals(DoubleWritable.class.toString())) {
                
		rank += dampingFactor * ((DoubleWritable) values.get()).get();
                
	    }
	    if (values.getDeclaredClass().toString().equals(String[].class.toString())) {
		urlList = (String[]) values.get(); 
	    }
	}
	rank = (1 - dampingFactor) + rank;
	context.write(key, new PageRank(rank, urlList));
    };
}
